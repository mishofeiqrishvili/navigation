package com.example.bottom_navigation_bar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.bottom_navigation_bar.fragments.DashboardFragment
import com.example.bottom_navigation_bar.fragments.FavoriteFragment
import com.example.bottom_navigation_bar.fragments.HomeFragment
import com.example.bottom_navigation_bar.fragments.NotificationFragment

private val Any.setOnNavigationItemSelectedListener: Unit
    get() {}

class MainActivity : AppCompatActivity() {
    private val dashboardFragment = DashboardFragment()
    private val homeFragment = HomeFragment()
    private val  notificationFragment = NotificationFragment()
    private val favoriteFragment = FavoriteFragment()
    private  var bottom_navigation = BottomNavigationView()

    private fun BottomNavigationView() {

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(dashboardFragment)

        bottom_navigation.setOnNavigationItemSelectedListener {it:MenuItem ->

        when (it.itemId){
            R.id.ic_dashboard -> replaceFragment(dashboardFragment)
            R.id.ic_home -> replaceFragment(homeFragment)
            R.id.ic_notification -> replaceFragment(notificationFragment)
            R.id.ic_favorite -> replaceFragment(favoriteFragment)
        }
            true


        }
    }

    private fun replaceFragment(fragment: Fragment){
        if(fragment!=null){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.commit()
        }
    }
}

private fun Unit.setOnNavigationItemSelectedListener(value: (MenuItem) -> Unit) {
    TODO("Not yet implemented")
}
